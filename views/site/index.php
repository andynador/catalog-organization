<?php

use yii\helpers\Html;
use yii\jui\AutoComplete;
use app\models\Rubric;
use yii\jui\JuiAsset;
use yii\helpers\Url;
use yii\web\JsExpression;
use kartik\select2\Select2;
/* @var $this yii\web\View */

$this->title = 'API Каталог Организаций';
JuiAsset::register($this);
$rubrics = Rubric::find()->select('name')->asArray(true)->column();
?>
<style type="text/css">
    .row {
        margin-bottom: 20px
    }
</style>
<?php 
    $this->registerJs(<<<JS
        
	$(function() {
		$('.action-form').submit(function(e) {
			e.preventDefault();
            if ($(this).attr('method') == 'get') {
                $.getJSON($(this).attr('action'), $(this).find('.active-element').serialize())
                    .done(function(data) {
                        $('#result').html(JSON.stringify(data, null, 4));
                    });
            } else {
                $.post($(this).attr('action'), $(this).find('.active-element').serialize(), function(data) {
                        $('#result').html(JSON.stringify(data, null, 4));
                    });
            }
		});
        $('#multiple-id-form').submit(function(e) {
			e.preventDefault();
            $.post($(this).attr('action'), {id: JSON.stringify($(this).find('.active-element').val())}, function(data) {
                $('#result').html(JSON.stringify(data, null, 4));
            });
        });
	});
JS
        )
?>
<div class="site-index">

    <div class="jumbotron">
        <h1><?= $this->title; ?></h1>
    </div>

    <div class="body-content">

        <div class="row" style="position:relative">
            <div class="col-sm-5">
            	<div class="row">
					<b>1) Выдача всех организаций, находящихся в конкретном здании</b>
					<?= Html::beginForm(['api/company-search/by-address'], 'get', ['class' => 'action-form']); ?>
    					<?= Html::textInput('q', null, ['placeholder' => 'Введите адрес для поиска', 'class' => 'form-control active-element']); ?>
    					<?= Html::submitInput('Получить', ['class' => 'btn btn-default']); ?>
					<?= Html::endForm(); ?>
            	</div>
            	
            	<div class="row">
					<b>2) Список всех организаций, которые относятся к указанной рубрике</b>
					<?= Html::beginForm(['api/company-search/by-rubric'], 'get', ['class' => 'action-form']); ?>
    					<?= AutoComplete::widget([
    					    'name' => 'q',
    					    'clientOptions' => [
    					        'source' => $rubrics,
    					    ],
    					    'options' => [
    					        'placeholder' => 'Введите рубрику для поиска', 'class' => 'form-control active-element'
    					   ]
    					]); ?>
    					<?= Html::submitInput('Получить', ['class' => 'btn btn-default']); ?>
					<?= Html::endForm(); ?>
            	</div>
            
            	<div class="row">
					<b>3) Список организаций, которые находятся в заданном радиусе/прямоугольной области относительно указанной точки на карте</b><br />
					<span>а. По радиусу и точке на карте</span>
					<?= Html::beginForm(['api/company-search/by-geodist'], 'get', ['class' => 'action-form']); ?>
    					<?= Html::textInput('lat', null, ['placeholder' => 'Введите широту, в градусах', 'class' => 'form-control active-element']); ?>
    					<?= Html::textInput('lon', null, ['placeholder' => 'Введите долготу, в градусах', 'class' => 'form-control active-element']); ?>
    					<?= Html::textInput('dist', null, ['placeholder' => 'Введите радиус, в метрах', 'class' => 'form-control active-element']); ?>
    					<?= Html::submitInput('Получить', ['class' => 'btn btn-default']); ?>
					<?= Html::endForm(); ?>
            	</div>
            	
            	<div class="row">
					<span>а. По прямоугольной области</span>
					<?= Html::beginForm(['api/company-search/by-geocoords'], 'get', ['class' => 'action-form']); ?>
    					<?= Html::textInput('x0', null, ['placeholder' => 'Введите широту левого верхнего угла области, в градусах', 'class' => 'form-control active-element']); ?>
    					<?= Html::textInput('y0', null, ['placeholder' => 'Введите долготу левого верхнего угла области, в градусах', 'class' => 'form-control active-element']); ?>
    					<?= Html::textInput('x1', null, ['placeholder' => 'Введите широту правого нижнего угла области, в градусах', 'class' => 'form-control active-element']); ?>
    					<?= Html::textInput('y1', null, ['placeholder' => 'Введите долготу правого нижнего угла области, в градусах', 'class' => 'form-control active-element']); ?>
    					<?= Html::submitInput('Получить', ['class' => 'btn btn-default']); ?>
					<?= Html::endForm(); ?>
            	</div>
            	
            	<div class="row">
					<b>4) Выдача информации об организациях по их идентификаторам</b>
					<?= Html::beginForm(['api/company-search'], 'post', ['id' => 'multiple-id-form']); ?>
    					<?= Select2::widget([
    					    'name' => 'id',
    					    'language' => 'ru',
    					    'showToggleAll' => false,
    					    'options' => [
    					        'placeholder' => 'Введите компанию для поиска (минимум - 2 символа)', 
    					        'class' => 'form-control active-element',
    					    ],
    					    'pluginOptions' => [
    					        'multiple' => true,
    					        'allowClear' => true,
    					        'minimumInputLength' => 2,
        					    'ajax' => [
        					        'url' => Url::to(['api/company-search/list-by-name']),
        					        'dataType' => 'json',
        					        'data' => new JsExpression('function(params) { return {q:params.term}; }'),
        					        'processResults' => new JsExpression('function (data) { 
        					            data.results = $.map(data.data, function (obj) {
                                            return obj;
                                        }); 
        					            return data; 
                                    }'
        					        ),
        					    ],
        					    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
        					    'templateResult' => new JsExpression('function(company) { return company.name; }'),
        					    'templateSelection' => new JsExpression('function (company) { return company.name; }'),
    					    ],
    					]); ?>
    					<?= Html::submitInput('Получить', ['class' => 'btn btn-default']); ?>
					<?= Html::endForm(); ?>
            	</div>
            	
            	<div class="row">
					<b>5) Поиск организации по названию</b>
					<?= Html::beginForm(['api/company-search/by-name'], 'get', ['class' => 'action-form']); ?>
    					<?= Select2::widget([
    					    'name' => 'company_name',
    					    'language' => 'ru',
    					    'showToggleAll' => false,
    					    'options' => [
    					        'placeholder' => 'Введите компанию для поиска (минимум - 2 символа)', 
    					        'class' => 'form-control active-element',
    					    ],
    					    'pluginOptions' => [
    					        'multiple' => false,
    					        'allowClear' => true,
    					        'minimumInputLength' => 2,
        					    'ajax' => [
        					        'url' => Url::to(['api/company-search/list-by-name']),
        					        'dataType' => 'json',
        					        'data' => new JsExpression('function(params) { return {q:params.term}; }'),
        					        'processResults' => new JsExpression('function (data) { 
        					            data.results = $.map(data.data, function (obj) {
                                            obj.id = obj.name;
                                            return obj;
                                        }); 
        					            return data; }'
        					        ),
        					    ],
        					    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
        					    'templateResult' => new JsExpression('function(company) { return company.name; }'),
        					    'templateSelection' => new JsExpression('function (company) { return company.name; }'),
    					    ],
    					]); ?>
    					<?= Html::submitInput('Получить', ['class' => 'btn btn-default']); ?>
					<?= Html::endForm(); ?>
            	</div>
            	
            	<div class="row">
					<b>6) Список зданий</b>
					<?= Html::beginForm(['api/structure-search/list'], 'get', ['class' => 'action-form']); ?>
    					<?= Html::submitInput('Получить', ['class' => 'btn btn-default']); ?>
					<?= Html::endForm(); ?>
            	</div>
            	
            	<div class="row">
					<b>7) Дерево рубрик каталога со всеми предками, с возможностью фильтрации по потомкам конкретного узла</b>
					<?= Html::beginForm(['api/rubric-search/by-name'], 'get', ['class' => 'action-form']); ?>
    					<?= AutoComplete::widget([
    					    'name' => 'q',
    					    'clientOptions' => [
    					        'source' => $rubrics,
    					    ],
    					    'options' => [
    					        'placeholder' => 'Введите рубрику для фильтрации', 'class' => 'form-control active-element'
    					   ]
    					]); ?>
    					<?= Html::submitInput('Получить', ['class' => 'btn btn-default']); ?>
					<?= Html::endForm(); ?>
            	</div>
            </div>
            <div class="col-sm-7">
            	<pre id="result" style="position: fixed; height: 600px; width: 700px ">Выберите метод из списка, чтобы посмотреть результат</pre>
            </div>
        </div>

    </div>
</div>
