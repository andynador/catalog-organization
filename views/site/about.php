<?php
use yii\helpers\Url;
/* @var $this yii\web\View */

$this->title = 'Документация';
?>
<style type="text/css">
    .annotation {
        background-color: #fffef2;
        border: 1px solid #faf6d4;
        margin-bottom: 25px;
        padding: 10px;
    }
    .doc-param {
        border-bottom: 1px dotted;
        color: #1a3dc1;
        position: relative;
        text-decoration: none;
    }
</style>
<div class="site-index">

    <div class="jumbotron">
        <h1><?= $this->title; ?></h1>
    </div>
    <div class="row">
    	<h2>Введение</h2>
    	API позволяет получить доступ к таким методам Каталога Организаций, как:
    	<ul>
    		<li>Список организаций, находящихся в конкретном здании</li>
    		<li>Поиск организаций по идентификатору или названию</li>
    		<li>Поиск организаций по гео-координатам</li>
    		<li>Поиск организаций по рубрике</li>
    		<li>Поиск рубрик по названию</li>
    		<li>Список зданий</li>
    	</ul>
    	<h3>Формат ответа</h3>
    	Данные возвращаются в виде JSON-объекта. Формат успешного ответа:
    	<pre class="annotation">
{
    <span class="doc-param">"success"</span>: true,
    <span class="doc-param">"data"</span>: &lt;json&gt;
}
    	</pre>
		Для успешного ответа поле <b>success</b> равно true. В поле <b>data</b> находятся данные, в зависимости от запрашиваемого метода.
		Формат неудачного выполнения запроса:
    	<pre class="annotation">
{
    <span class="doc-param">"success"</span>: false,
    <span class="doc-param">"message"</span>: &lt;string&gt;
}
    	</pre>
		При неудачном выполнении запроса поле "success" равно false. В поле "message" содержится сообщение с ошибкой. Ошибка может быть вызвана,
		если метод запрошен с неверным типом запроса, либо отсутствуют обязательные параметры запроса.
    	<h2>Список методов</h2>
    	
    	<h3>Выдача всех организаций, находящихся в конкретном здании</h4>
    	Возвращает компании, находящиеся по указанному адресу
    	<h4>Синтаксис запроса</h4>
    	<pre class="annotation">
GET <?= Url::to(['api/company-search/by-address'], true); ?> ?
[<span class="doc-param">q</span>=&lt;string&gt;]
    	</pre>
    	<table class="table table-bordered">
    		<thead>
        		<tr>
        			<th>Параметр</th><th>Тип</th><th>Описание</th><th>Обязательный</th>
        		</tr>
    		</thead>
    		<tbody>
    			<tr>
    				<td>q</td><td>Строка</td><td>Адрес для поиска организаций</td><td>Да</td>
    			</tr>
    		</tbody>
    	</table>
    	
    	<h4>Формат ответа</h4>
    	<pre class="annotation">
{
    "success": true,
    "data": [
        {
            "lat": "42.324459",
            "lon": "-142.541733",
            "name": "ЗАО ТехАсбоцементСтрой",
            "phones": "(495) 747-1448, (495) 191-1019, (495) 108-1345",
            "address": "ул. Гоголя, 28",
            "rubrics": "Запчасти к сельхозтехнике"
        }
    ]
}
    	</pre>
    	Ключ <b>data</b> содержит массив с компаниями, удовлетворяющих
    	фильтру по адресу. Поля компании возвращаются следующие:
    	<table class="table table-bordered">
    		<thead>
        		<tr>
        			<th>Ключ</th><th>Тип</th><th>Описание</th>
        		</tr>
    		</thead>
    		<tbody>
    			<tr>
    				<td>lat</td><td>Число</td><td>Широта</td>
    			</tr>
    			<tr>
    				<td>lon</td><td>Число</td><td>Долгота</td>
    			</tr>
    			<tr>
    				<td>name</td><td>Строка</td><td>Название компании</td>
    			</tr>
    			<tr>
    				<td>phones</td><td>Строка</td><td>Телефоны</td>
    			</tr>
    			<tr>
    				<td>address</td><td>Строка</td><td>Адрес</td>
    			</tr>
    			<tr>
    				<td>rubrics</td><td>Строка</td><td>Рубрики</td>
    			</tr>
    		</tbody>
    	</table>
    	
    	<h3>Список всех организаций, которые относятся к указанной рубрике</h4>
    	Возвращает компании, относящиеся к указанной рубрике
    	<h4>Синтаксис запроса</h4>
    	<pre class="annotation">
GET <?= Url::to(['api/company-search/by-rubric'], true); ?> ?
[<span class="doc-param">q</span>=&lt;string&gt;]
    	</pre>
    	<table class="table table-bordered">
    		<thead>
        		<tr>
        			<th>Параметр</th><th>Тип</th><th>Описание</th><th>Обязательный</th>
        		</tr>
    		</thead>
    		<tbody>
    			<tr>
    				<td>q</td><td>Строка</td><td>Рубрика для поиска организаций</td><td>Да</td>
    			</tr>
    		</tbody>
    	</table>
    	
    	<h4>Формат ответа</h4>
    	<pre class="annotation">
{
    "success": true,
    "data": [
        {
            "lat": "42.324459",
            "lon": "-142.541733",
            "name": "ЗАО ТехАсбоцементСтрой",
            "phones": "(495) 747-1448, (495) 191-1019, (495) 108-1345",
            "address": "ул. Гоголя, 28",
            "rubrics": "Запчасти к сельхозтехнике"
        },
        {
            "lat": "-67.999573",
            "lon": "175.702301",
            "name": "ОАО АлмазТранс",
            "phones": "(35222) 21-1449, (812) 793-37-18",
            "address": "бульвар Ломоносова, 47",
            "rubrics": "Ремонт / заправка автокондиционеров"
        },
    ]
}
    	</pre>
    	Ключ <b>data</b> содержит массив с компаниями, которые отностятся к указанной рубрике. Поля компании возвращаются следующие:
    	<table class="table table-bordered">
    		<thead>
        		<tr>
        			<th>Ключ</th><th>Тип</th><th>Описание</th>
        		</tr>
    		</thead>
    		<tbody>
    			<tr>
    				<td>lat</td><td>Число</td><td>Широта</td>
    			</tr>
    			<tr>
    				<td>lon</td><td>Число</td><td>Долгота</td>
    			</tr>
    			<tr>
    				<td>name</td><td>Строка</td><td>Название компании</td>
    			</tr>
    			<tr>
    				<td>phones</td><td>Строка</td><td>Телефоны</td>
    			</tr>
    			<tr>
    				<td>address</td><td>Строка</td><td>Адрес</td>
    			</tr>
    			<tr>
    				<td>rubrics</td><td>Строка</td><td>Рубрики</td>
    			</tr>
    		</tbody>
    	</table>
    	
    	<h3>Список организаций, которые находятся в заданном радиусе относительно точки</h4>
    	Возвращает компании, находящихся в радиусе относительно точки
    	<h4>Синтаксис запроса</h4>
    	<pre class="annotation">
GET <?= Url::to(['api/company-search/by-geodist'], true); ?> ?
[<span class="doc-param">lat</span>=&lt;float&gt;] &
[<span class="doc-param">lon</span>=&lt;float&gt;] &
[<span class="doc-param">dist</span>=&lt;float&gt;]
    	</pre>
    	<table class="table table-bordered">
    		<thead>
        		<tr>
        			<th>Параметр</th><th>Тип</th><th>Описание</th><th>Обязательный</th>
        		</tr>
    		</thead>
    		<tbody>
    			<tr>
    				<td>lat</td><td>Число</td><td>Широта, в градусах</td><td>Да</td>
    			</tr>
    			<tr>
    				<td>lon</td><td>Число</td><td>Долгота, в градусах</td><td>Да</td>
    			</tr>
    			<tr>
    				<td>dist</td><td>Число</td><td>Радиус, в метрах</td><td>Да</td>
    			</tr>
    		</tbody>
    	</table>
    	
    	<h4>Формат ответа</h4>
    	<pre class="annotation">
{
    "success": true,
    "data": [
        {
            "lat": "42.324459",
            "lon": "-142.541733",
            "name": "ЗАО ТехАсбоцементСтрой",
            "phones": "(495) 747-1448, (495) 191-1019, (495) 108-1345",
            "address": "ул. Гоголя, 28",
            "rubrics": "Запчасти к сельхозтехнике"
        }
    ]
}
    	</pre>
    	Ключ <b>data</b> содержит массив с компаниями, находящихся в радиусе относительно точки. Поля компании возвращаются следующие:
    	<table class="table table-bordered">
    		<thead>
        		<tr>
        			<th>Ключ</th><th>Тип</th><th>Описание</th>
        		</tr>
    		</thead>
    		<tbody>
    			<tr>
    				<td>lat</td><td>Число</td><td>Широта</td>
    			</tr>
    			<tr>
    				<td>lon</td><td>Число</td><td>Долгота</td>
    			</tr>
    			<tr>
    				<td>name</td><td>Строка</td><td>Название компании</td>
    			</tr>
    			<tr>
    				<td>phones</td><td>Строка</td><td>Телефоны</td>
    			</tr>
    			<tr>
    				<td>address</td><td>Строка</td><td>Адрес</td>
    			</tr>
    			<tr>
    				<td>rubrics</td><td>Строка</td><td>Рубрики</td>
    			</tr>
    		</tbody>
    	</table>
    	
    	<h3>Список организаций, которые находятся в прямоугольной области</h4>
    	Возвращает компании, находящихся в указанной прямоугольной области
    	<h4>Синтаксис запроса</h4>
    	<pre class="annotation">
GET <?= Url::to(['api/company-search/by-geocoords'], true); ?> ?
[<span class="doc-param">x0</span>=&lt;float&gt;] &
[<span class="doc-param">y0</span>=&lt;float&gt;] &
[<span class="doc-param">x1</span>=&lt;float&gt;] &
[<span class="doc-param">y1</span>=&lt;float&gt;] 
    	</pre>
    	<table class="table table-bordered">
    		<thead>
        		<tr>
        			<th>Параметр</th><th>Тип</th><th>Описание</th><th>Обязательный</th>
        		</tr>
    		</thead>
    		<tbody>
    			<tr>
    				<td>x0</td><td>Число</td><td>Широта левого верхнего угла области, в градусах</td><td>Да</td>
    			</tr>
    			<tr>
    				<td>y0</td><td>Число</td><td>Долгота левого верхнего угла области, в градусах</td><td>Да</td>
    			</tr>
    			<tr>
    				<td>x1</td><td>Число</td><td>Широта правого нижнего угла области, в градусах</td><td>Да</td>
    			</tr>
    			<tr>
    				<td>y1</td><td>Число</td><td>Долгота правого нижнего угла области, в градусах</td><td>Да</td>
    			</tr>
    		</tbody>
    	</table>
    	
    	<h4>Формат ответа</h4>
    	<pre class="annotation">
{
    "success": true,
    "data": [
        {
            "lat": "42.324459",
            "lon": "-142.541733",
            "name": "ЗАО ТехАсбоцементСтрой",
            "phones": "(495) 747-1448, (495) 191-1019, (495) 108-1345",
            "address": "ул. Гоголя, 28",
            "rubrics": "Запчасти к сельхозтехнике"
        }
    ]
}
    	</pre>
    	Ключ <b>data</b> содержит массив с компаниями, находящихся в указанной прямоугольной области. Поля компании возвращаются следующие:
    	<table class="table table-bordered">
    		<thead>
        		<tr>
        			<th>Ключ</th><th>Тип</th><th>Описание</th>
        		</tr>
    		</thead>
    		<tbody>
    			<tr>
    				<td>lat</td><td>Число</td><td>Широта</td>
    			</tr>
    			<tr>
    				<td>lon</td><td>Число</td><td>Долгота</td>
    			</tr>
    			<tr>
    				<td>name</td><td>Строка</td><td>Название компании</td>
    			</tr>
    			<tr>
    				<td>phones</td><td>Строка</td><td>Телефоны</td>
    			</tr>
    			<tr>
    				<td>address</td><td>Строка</td><td>Адрес</td>
    			</tr>
    			<tr>
    				<td>rubrics</td><td>Строка</td><td>Рубрики</td>
    			</tr>
    		</tbody>
    	</table>
    	
    	<h3>Выдача информации об организациях по их идентификаторам</h4>
    	Возвращает компании с указанными идентификаторами
    	<h4>Синтаксис запроса</h4>
    	<pre class="annotation">
POST <?= Url::to(['api/company-search/'], true); ?>

[<span class="doc-param">id</span>=&lt;array&gt;]
    	</pre>
    	<table class="table table-bordered">
    		<thead>
        		<tr>
        			<th>Параметр</th><th>Тип</th><th>Описание</th><th>Обязательный</th>
        		</tr>
    		</thead>
    		<tbody>
    			<tr>
    				<td>id</td><td>Массив в JSON формате</td><td>Идентификаторы команий</td><td>Да</td>
    			</tr>
    		</tbody>
    	</table>
    	
    	<h4>Формат ответа</h4>
    	<pre class="annotation">
{
    "success": true,
    "data": [
        {
            "lat": "42.324459",
            "lon": "-142.541733",
            "name": "ЗАО ТехАсбоцементСтрой",
            "phones": "(495) 747-1448, (495) 191-1019, (495) 108-1345",
            "address": "ул. Гоголя, 28",
            "rubrics": "Запчасти к сельхозтехнике"
        }
    ]
}
    	</pre>
    	Ключ <b>data</b> содержит массив с компаниями с указанными идентификаторами. Поля компании возвращаются следующие:
    	<table class="table table-bordered">
    		<thead>
        		<tr>
        			<th>Ключ</th><th>Тип</th><th>Описание</th>
        		</tr>
    		</thead>
    		<tbody>
    			<tr>
    				<td>lat</td><td>Число</td><td>Широта</td>
    			</tr>
    			<tr>
    				<td>lon</td><td>Число</td><td>Долгота</td>
    			</tr>
    			<tr>
    				<td>name</td><td>Строка</td><td>Название компании</td>
    			</tr>
    			<tr>
    				<td>phones</td><td>Строка</td><td>Телефоны</td>
    			</tr>
    			<tr>
    				<td>address</td><td>Строка</td><td>Адрес</td>
    			</tr>
    			<tr>
    				<td>rubrics</td><td>Строка</td><td>Рубрики</td>
    			</tr>
    		</tbody>
    	</table>
    	
    	<h3>Поиск организации по названию</h4>
    	Возвращает компанию с указанным названием
    	<h4>Синтаксис запроса</h4>
    	<pre class="annotation">
GET <?= Url::to(['api/company-search/by-name'], true); ?>?
[<span class="doc-param">company_name</span>=&lt;string&gt;]
    	</pre>
    	<table class="table table-bordered">
    		<thead>
        		<tr>
        			<th>Параметр</th><th>Тип</th><th>Описание</th><th>Обязательный</th>
        		</tr>
    		</thead>
    		<tbody>
    			<tr>
    				<td>company_name</td><td>Строка</td><td>Название компании</td><td>Да</td>
    			</tr>
    		</tbody>
    	</table>
    	
    	<h4>Формат ответа</h4>
    	<pre class="annotation">
{
    "success": true,
    "data": {
        "name": "ООО КазаньЭлектроРос",
        "phones": "8-800-812-9742",
        "address": "наб. Гоголя, 36",
        "lat": -34.954155,
        "lon": -10.146617,
        "rubrics": "Аппаратная замена масла"
    }
}
    	</pre>
    	Ключ <b>data</b> содержит объект с компанией с указанным именем. Поля компании возвращаются следующие:
    	<table class="table table-bordered">
    		<thead>
        		<tr>
        			<th>Ключ</th><th>Тип</th><th>Описание</th>
        		</tr>
    		</thead>
    		<tbody>
    			<tr>
    				<td>lat</td><td>Число</td><td>Широта</td>
    			</tr>
    			<tr>
    				<td>lon</td><td>Число</td><td>Долгота</td>
    			</tr>
    			<tr>
    				<td>name</td><td>Строка</td><td>Название компании</td>
    			</tr>
    			<tr>
    				<td>phones</td><td>Строка</td><td>Телефоны</td>
    			</tr>
    			<tr>
    				<td>address</td><td>Строка</td><td>Адрес</td>
    			</tr>
    			<tr>
    				<td>rubrics</td><td>Строка</td><td>Рубрики</td>
    			</tr>
    		</tbody>
    	</table>
    	
    	<h3>Список зданий</h4>
    	Возвращает все здания
    	<h4>Синтаксис запроса</h4>
    	<pre class="annotation">
GET <?= Url::to(['api/structure-search/list'], true); ?>?
    	</pre>
    	
    	<h4>Формат ответа</h4>
    	<pre class="annotation">
{
    "success": true,
    "data": [
        {
            "address": "въезд Ленина, 62",
            "lat": "77.102391",
            "lon": "110.821207"
        },
        {
            "address": "въезд Бухарестская, 95",
            "lat": "34.311027",
            "lon": "-142.574049"
        },
        {
            "address": "проезд Будапештсткая, 33",
            "lat": "-14.69593",
            "lon": "109.138564"
        },
        {
            "address": "пр. Гагарина, 92",
            "lat": "-9.848094",
            "lon": "-166.030251"
        }
    ]
}
    	</pre>
    	Ключ <b>data</b> содержит массив со зданиями. Поля здания возвращаются следующие:
    	<table class="table table-bordered">
    		<thead>
        		<tr>
        			<th>Ключ</th><th>Тип</th><th>Описание</th>
        		</tr>
    		</thead>
    		<tbody>
    			<tr>
    				<td>lat</td><td>Число</td><td>Широта</td>
    			</tr>
    			<tr>
    				<td>lon</td><td>Число</td><td>Долгота</td>
    			</tr>
    			<tr>
    				<td>address</td><td>Строка</td><td>Адрес</td>
    			</tr>
    		</tbody>
    	</table>
    	
    	<h3>Дерево рубрик каталога со всеми предками</h4>
    	Возвращает дерево рубрик каталога со всеми предками, с возможностью фильтрации по потомкам конкретного узла
    	<h4>Синтаксис запроса</h4>
    	<pre class="annotation">
GET <?= Url::to(['api/rubric-search/by-name'], true); ?> ?
[<span class="doc-param">q</span>=&lt;string&gt;]
    	</pre>
    	<table class="table table-bordered">
    		<thead>
        		<tr>
        			<th>Параметр</th><th>Тип</th><th>Описание</th><th>Обязательный</th>
        		</tr>
    		</thead>
    		<tbody>
    			<tr>
    				<td>q</td><td>Строка</td><td>Рубрика для поиска</td><td>Нет</td>
    			</tr>
    		</tbody>
    	</table>
    	
    	<h4>Формат ответа</h4>
    	<pre class="annotation">
{
    "success": true,
    "data": [
        {
            "name": "Шины / Диски",
            "items": [
                {
                    "name": "Шины",
                    "items": []
                },
                {
                    "name": "Диски",
                    "items": []
                }
            ]
        }
    ]
}
    	</pre>
    	Ключ <b>data</b> содержит массив с компаниями, которые отностятся к указанной рубрике. Поля компании возвращаются следующие:
    	<table class="table table-bordered">
    		<thead>
        		<tr>
        			<th>Ключ</th><th>Тип</th><th>Описание</th>
        		</tr>
    		</thead>
    		<tbody>
    			<tr>
    				<td>name</td><td>Строка</td><td>Название рубрики</td>
    			</tr>
    			<tr>
    				<td>items</td><td>Массив</td><td>Вложенные рубрики</td>
    			</tr>
    		</tbody>
    	</table>
    </div>
</div>