<?php
namespace app\models;

use Yii;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "company".
 *
 * @property integer $id
 * @property string $name
 * @property string $phones
 * @property integer $structure_id
 *
 * @property Structure $structure
 * @property CompanyRubric[] $companyRubrics
 * @property Rubric[] $rubrics
 */
class Company extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'name',
                    'structure_id'
                ],
                'required'
            ],
            [
                [
                    'phones'
                ],
                'safe'
            ],
            [
                [
                    'structure_id'
                ],
                'integer'
            ],
            [
                [
                    'name'
                ],
                'string',
                'max' => 255
            ],
            [
                [
                    'name'
                ],
                'unique'
            ],
            [
                [
                    'structure_id'
                ],
                'exist',
                'skipOnError' => true,
                'targetClass' => Structure::className(),
                'targetAttribute' => [
                    'structure_id' => 'id'
                ]
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'phones' => 'Phones',
            'structure_id' => 'Structure ID'
        ];
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStructure()
    {
        return $this->hasOne(Structure::className(), [
            'id' => 'structure_id'
        ]);
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyRubrics()
    {
        return $this->hasMany(CompanyRubric::className(), [
            'company_id' => 'id'
        ]);
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRubrics()
    {
        return $this->hasMany(Rubric::className(), [
            'id' => 'rubric_id'
        ])->viaTable('company_rubric', [
            'company_id' => 'id'
        ]);
    }

    /**
     * Метод, сохраняющий модель в индексе Sphinx.
     */
    public function saveModelInSphinxIndex()
    {
        $attributes = $this->getAttributes(null, [
            'structure_id'
        ]);
        $attributes['phones'] = implode(', ', Json::decode($this->phones));
        $attributes['address'] = $this->structure->address;
        $attributes['rubrics'] = implode(', ', ArrayHelper::getColumn($this->rubrics, 'name'));
        $attributes['lat'] = $this->structure->lat;
        $attributes['lon'] = $this->structure->lon;
        /*
         * Также сохраним широту и долготу в радианах - эти данные необходимы для проверки
         * принадлежности гео-координат в радиусе от указанной точки
         */
        $attributes['lat_radians'] = deg2rad($this->structure->lat);
        $attributes['lon_radians'] = deg2rad($this->structure->lon);
        Yii::$app->sphinx->createCommand()->replace(CompanyRT::indexName(), $attributes)->execute();
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see \yii\db\BaseActiveRecord::afterSave($insert, $changedAttributes)
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        $this->saveModelInSphinxIndex();
    }

    /**
     * Метод, удаляющий текущую модель из индекса Sphinx
     */
    public function deleteModelFromSphinxIndex()
    {
        $modelIndex = CompanyRT::findOne($this->id);
        if ($modelIndex) {
            $modelIndex->delete();
        }
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see \yii\db\BaseActiveRecord::afterDelete()
     */
    public function afterDelete()
    {
        parent::afterDelete();
        $this->deleteModelFromSphinxIndex();
    }
}
