<?php
namespace app\models;

use Yii;

/**
 * This is the model class for table "structure".
 *
 * @property integer $id
 * @property string $address
 * @property double $lat
 * @property double $lon
 *
 * @property Company[] $companies
 */
class Structure extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'structure';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'address'
                ],
                'required'
            ],
            [
                [
                    'lat',
                    'lon'
                ],
                'number'
            ],
            [
                [
                    'address'
                ],
                'string',
                'max' => 255
            ],
            [
                [
                    'address'
                ],
                'unique'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'address' => 'Address',
            'lat' => 'Lat',
            'lon' => 'Lon'
        ];
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompanies()
    {
        return $this->hasMany(Company::className(), [
            'structure_id' => 'id'
        ]);
    }
}
