<?php
namespace app\models;

use creocoder\nestedsets\NestedSetsQueryBehavior;

/**
 * This is the ActiveQuery class for [[Rubric]].
 *
 * @see Rubric
 */
class RubricQuery extends \yii\db\ActiveQuery
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            NestedSetsQueryBehavior::className()
        ];
    }

    /**
     * Метод, добавляющий условие на поиск рубрики по названию
     * 
     * @param string $name название
     * @return \app\models\RubricQuery объект для формирования запросов
     */
    public function byName($name)
    {
        $this->andFilterWhere([
            'name' => $name
        ]);
        return $this;
    }

    /**
     * @inheritdoc
     *
     * @return Rubric[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     *
     * @return Rubric|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
