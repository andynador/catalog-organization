<?php
namespace app\models;

use yii\sphinx\ActiveRecord;

/**
 * This is the model class for index "company_rt".
 *
 * @property integer $id
 * @property string $name
 * @property string $phones
 * @property string $address
 * @property string $rubrics
 * @property float $lat
 * @property float $lon
 * @property float $lat_radias
 * @property float $lon_radias
 */
class CompanyRT extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function indexName()
    {
        return 'company_rt';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'name'
                ],
                'required'
            ],
            [
                [
                    'name',
                    'phones',
                    'address',
                    'rubrics'
                ],
                'string'
            ],
            [
                [
                    'lat',
                    'lon',
                    'lat_radias',
                    'lon_radias'
                ],
                'numeric'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'phones' => 'Phones',
            'address' => 'Address',
            'rubrics' => 'Rubrics',
            'lat' => 'Lat',
            'lon' => 'Lon'
        ];
    }
}