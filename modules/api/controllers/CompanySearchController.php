<?php
namespace app\modules\api\controllers;

use Yii;
use \app\modules\api\base\ApiController;
use app\models\CompanyRT;
use yii\sphinx\ActiveQuery;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * Контроллер для API получения данных по компаниям
 *
 * @author Andy
 */
class CompanySearchController extends ApiController
{

    /**
     *
     * @var sting исполняемый по умолчанию action
     */
    public $defaultAction = 'by-id';

    /**
     *
     * @var array список аттрибутов, которые будут возвращаться в ответе на запрос
     */
    protected $columns = [
        'name',
        'phones',
        'address',
        'lat',
        'lon',
        'rubrics',
    ];

    /**
     * Метод, устанавливающи параметры для выборки компании
     *
     * @return ActiveQuery объект с параметрами выборки
     */
    protected function getQueryForSelectCompany()
    {
        return CompanyRT::find()->limit(1000)->asArray();
    }

    /**
     * Метод, возвращающий из выборки по компаниям только нужные поля
     *
     * @param array $data массив с компаниями, из которого нужно выбрать нужные поля
     * @param array $additionaColumns дополнительные колонки для выборки
     * @return array массив с компаниями, содержащими нужные поля
     */
    protected function getColumnsFromData($data, $additionaColumns = [])
    {
        $coulmns = array_merge($this->columns, $additionaColumns);
        $columns = array_combine($coulmns, $coulmns);
        
        return array_values(ArrayHelper::map($data, 'id', function ($item) use($columns) {
            return array_intersect_key($item, $columns);
        }));
    }

    /**
     * Метод, возвращающий компании по переданному адресу
     */
    public function actionByAddress()
    {
        if (!Yii::$app->request->isGet) {
            $this->sendErrorResponse('Неверный тип запроса');
        }
        
        $q = Yii::$app->request->get('q');
        if ($q == '') {
            $this->sendErrorResponse('Не указан параметр для поиска q');
        }
        
        $data = $this->getQueryForSelectCompany()->match(new Expression(':match', [
            'match' => '@(address) ' . Yii::$app->sphinx->escapeMatchValue($q)
        ]))->all();
        
        $this->sendSuccessResponse($this->getColumnsFromData($data));
    }

    /**
     * Метод, возвращающий компании, которые относятся к указанной рубрике
     */
    public function actionByRubric()
    {
        if (!Yii::$app->request->isGet) {
            $this->sendErrorResponse('Неверный тип запроса');
        }
        
        $q = Yii::$app->request->get('q');
        if ($q == '') {
            $this->sendErrorResponse('Не указан параметр для поиска q');
        }
        
        $data = $this->getQueryForSelectCompany()->match(new Expression(':match', [
            'match' => '@(rubrics) ' . Yii::$app->sphinx->escapeMatchValue($q)
        ]))->all();
        
        $this->sendSuccessResponse($this->getColumnsFromData($data));
    }

    /**
     * Метод, возвращающий компании, которые находятся в заданном радиусе относительно указанной
     * точки на карте
     */
    public function actionByGeodist()
    {
        if (!Yii::$app->request->isGet) {
            $this->sendErrorResponse('Неверный тип запроса');
        }
        
        $lat = Yii::$app->request->get('lat');
        if ($lat == '') {
            $this->sendErrorResponse('Не указан параметр для поиска lat');
        }
        
        $lon = Yii::$app->request->get('lon');
        if ($lon == '') {
            $this->sendErrorResponse('Не указан параметр для поиска lon');
        }
        
        $dist = Yii::$app->request->get('dist');
        if ($dist == '') {
            $this->sendErrorResponse('Не указан параметр для поиска dist');
        }
        
        $lat = deg2rad(floatval($lat));
        $lon = deg2rad(floatval($lon));
        $dist = floatval($dist);
        
        $data = $this->getQueryForSelectCompany()->addSelect([
            '*',
            new Expression("GEODIST($lat, $lon, lat_radians, lon_radians) AS dist")
        ])->andWhere("dist < $dist")->all();
        
        $this->sendSuccessResponse($this->getColumnsFromData($data));
    }

    /**
     * Метод, возвращающий компании, которые находятся в в указанной прямоугольной области
     */
    public function actionByGeocoords()
    {
        if (!Yii::$app->request->isGet) {
            $this->sendErrorResponse('Неверный тип запроса');
        }
        
        $x0 = Yii::$app->request->get('x0');
        if ($x0 == '') {
            $this->sendErrorResponse('Не указан параметр для поиска x0');
        }
        $x0 = floatval($x0);
        
        $y0 = Yii::$app->request->get('y0');
        if ($y0 == '') {
            $this->sendErrorResponse('Не указан параметр для поиска y0');
        }
        $y0 = floatval($y0);
        
        $x1 = Yii::$app->request->get('x1');
        if ($x1 == '') {
            $this->sendErrorResponse('Не указан параметр для поиска x1');
        }
        $x1 = floatval($x1);
        
        $y1 = Yii::$app->request->get('y1');
        if ($y1 == '') {
            $this->sendErrorResponse('Не указан параметр для поиска y1');
        }
        $y1 = floatval($y1);
        
        $data = $this->getQueryForSelectCompany()->addSelect([
            '*',
            new Expression("CONTAINS(POLY2D($x0, $y0, $x1, $y0, $x1, $y0, $x1, $y1), lat, lon) AS inside")
        ])->andWhere([
            'inside' => 1
        ])->all();
        
        $this->sendSuccessResponse($this->getColumnsFromData($data));
    }

    /**
     * Метод, возвращающий список компаний по их идентификаторам
     */
    public function actionById()
    {
        if (!Yii::$app->request->isPost) {
            $this->sendErrorResponse('Неверный тип запроса');
        }
        
        $id = Yii::$app->request->post('id');
        if (empty($id)) {
            $this->sendErrorResponse('Не указан параметр для поиска id');
        }
        $id = Json::decode($id);
        
        $data = CompanyRT::find()->asArray()->where([
            'id' => $id
        ])->all();
        $this->sendSuccessResponse($this->getColumnsFromData($data));
    }

    /**
     * Метод, возвращающий компанию по названию
     */
    public function actionByName()
    {
        if (!Yii::$app->request->isGet) {
            $this->sendErrorResponse('Неверный тип запроса');
        }
        
        $company_name = Yii::$app->request->get('company_name');
        if ($company_name == '') {
            $this->sendErrorResponse('Не указан параметр для поиска company_name');
        }
        
        $data = $this->getQueryForSelectCompany()->where([
            'name' => $company_name
        ])->asArray(false)->one();
        
        if ($data) {
            $data = $data->getAttributes($this->columns);
        }
        
        $this->sendSuccessResponse($data);
    }

    /**
     * Метод, возвращающий список компаний по названию
     */
    public function actionListByName()
    {
        if (!Yii::$app->request->isGet) {
            $this->sendErrorResponse('Неверный тип запроса');
        }
        
        $q = Yii::$app->request->get('q');
        
        $data = $this->getQueryForSelectCompany()->addSelect([
            '*',
            'id'
        ])->match(new Expression(':match', [
            'match' => '@(name) ' . Yii::$app->sphinx->escapeMatchValue($q)
        ]))->all();
        
        $this->sendSuccessResponse($this->getColumnsFromData($data, [
            'id'
        ]));
    }
}
