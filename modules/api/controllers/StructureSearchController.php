<?php
namespace app\modules\api\controllers;

use \app\modules\api\base\ApiController;
use app\models\Structure;

/**
 * Контроллер для API получения данных по зданиям
 *
 * @author Andy
 */
class StructureSearchController extends ApiController
{

    /**
     * Метод, возвращающий все здания
     */
    public function actionList()
    {
        $data = Structure::find()->select([
            'address',
            'lat',
            'lon'
        ])->asArray(true)->all();
        
        $this->sendSuccessResponse($data);
    }
}