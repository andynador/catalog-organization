<?php
namespace app\modules\api\controllers;

use Yii;
use \app\modules\api\base\ApiController;
use app\models\Rubric;

/**
 * Контроллер для API получения данных по рубрикам
 *
 * @author Andy
 */
class RubricSearchController extends ApiController
{

    /**
     *
     * @var array массив с рубриками, добавленными в дерево
     */
    protected $proccessed = [];

    /**
     * Метод, строящий рекурсивно дерево вложенных рубрик для переданной рубрики
     *
     * @param Rubric $model рубрика, для которой нужно добавить дочерние рубрики
     */
    protected function buildTree($model)
    {
        if (in_array($model->id, $this->proccessed)) {
            return [];
        }
        $this->proccessed[] = $model->id;
        
        $data = [
            'name' => $model->name,
            'items' => []
        ];
        
        $childrens = $model->children()->andWhere([
            'depth' => $model->depth + 1,
            'parent_id' => $model->parent_id
        ])->all();
        
        foreach ($childrens as $children) {
            $data['items'] = array_merge($data['items'], [
                $this->buildTree($children)
            ]);
        }
        
        return $data;
    }

    /**
     * Метод, возвращающий рубрику по названию
     */
    public function actionByName()
    {
        $q = Yii::$app->request->get('q');
        $models = Rubric::find()->byName($q)->orderBy([
            'parent_id' => SORT_ASC,
            'lft' => SORT_ASC
        ])->all();
        
        $data = [];
        
        foreach ($models as $model) {
            $temp = $this->buildTree($model);
            if ($temp) {
                $data[] = $temp;
            }
        }
        $this->sendSuccessResponse($data);
    }
}