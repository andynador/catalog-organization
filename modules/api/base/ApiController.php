<?php
namespace app\modules\api\base;

use Yii;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\ContentNegotiator;

/**
 * Базовый класс для контроллеров, работающих с API сущностей
 * 
 * @author Andy
 */
abstract class ApiController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'bootstrap' => [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON
                ],
                'languages' => [
                    'ru'
                ]
            ]
        ];
    }

    /**
     * Метод, отправляющий сообщение об ошибке клиенту
     *
     * @param string $message сообщение об ошибке
     */
    public function sendErrorResponse($message)
    {
        Yii::$app->response->data = [
            'success' => false,
            'message' => $message
        ];
        Yii::$app->end();
    }

    /**
     * Метод, отправляющий данные в случае успешного выполнения запроса
     *
     * @param mixed $data данные для отправки
     */
    public function sendSuccessResponse($data)
    {
        $result = [
            'success' => true,
            'data' => $data
        ];
        Yii::$app->response->data = $result;
        Yii::$app->end();
    }
}