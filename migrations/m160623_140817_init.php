<?php

use yii\db\Migration;

class m160623_140817_init extends Migration
{
    public function safeUp()
    {
        $this->createTable('structure', [
            'id' => $this->primaryKey(),
            'address' => $this->string()->notNull()->unique(),
            'lat' => $this->double(),
            'lon' => $this->double(),
        ]);
        
        $this->createTable('company', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()->unique(),
            'phones' => 'jsonb',
            'structure_id' => $this->integer()->notNull(),
        ]);
        
        $this->addForeignKey('fk_company_structure_id', 'company', 'structure_id', 'structure', 'id', 'CASCADE', 'CASCADE');
        
        $this->createTable('rubric', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()->unique(),
            'parent_id' => $this->integer(),
            'lft' => $this->integer()->notNull(),
            'rgt' => $this->integer()->notNull(),
            'depth' => $this->integer()->notNull(),
        ]);
        
        $this->createTable('company_rubric', [
           'company_id' => $this->integer()->notNull(), 
           'rubric_id' => $this->integer()->notNull(), 
        ]);
        
        $this->addPrimaryKey('pk_company_rubric', 'company_rubric', ['company_id', 'rubric_id']);
        $this->addForeignKey('fk_company_rubric_company_id', 'company_rubric', 'company_id', 'company', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_company_rubric_rubric_id', 'company_rubric', 'rubric_id', 'rubric', 'id', 'CASCADE', 'CASCADE');
        
    }

    public function safeDown()
    {
        $this->dropTable('company_rubric');
        $this->dropTable('rubric');
        $this->dropTable('company');
        $this->dropTable('structure');
        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
