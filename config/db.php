<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'pgsql:host=localhost;dbname=mini2gis',
    'username' => 'postgres',
    'password' => 'postgres',
    'charset' => 'utf8',
];
