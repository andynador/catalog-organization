<?php
namespace app\commands;

use Yii;
use app\models\Structure;
use app\models\Company;
use Faker\Generator;
use yii\helpers\Json;
use yii\console\Controller;
use app\models\Rubric;
use app\models\CompanyRT;

/**
 * Консольная команда для генерации тестовых данных
 *
 * @author Andy
 */
class FillDataController extends Controller
{

    const MAX_STRUCTURE = 100000;

    const MAX_COMPANY = 100000;

    CONST MAX_PHONES = 4;

    CONST MAX_RUBRICS_FOR_COMPANY = 3;

    /**
     *
     * @var integer $structureCount сколько зданий нужно сгенерировать
     */
    public $structureCount;

    /**
     *
     * @var integer $companyCount сколько компаний нужно сгенерировать
     */
    public $companyCount;

    /**
     * @inheritdoc
     */
    public function options($actionID)
    {
        return array_merge(parent::options($actionID), ($actionID == 'index' || $actionID == 'add-new-structures-and-companies') ? [
            'structureCount',
            'companyCount'
        ] : []);
    }

    /**
     *
     * @var Generator генератор случайных значений
     */
    protected $faker;

    /**
     * Метод, генерирующий тестовые данные для Зданий
     */
    protected function generateStructures()
    {
        Yii::$app->db->createCommand('TRUNCATE TABLE ' . Structure::tableName() . ' CASCADE')->execute();
        Yii::$app->db->createCommand('ALTER SEQUENCE structure_id_seq RESTART WITH 1')->execute();
        
        echo "    > Generate Structures\n";
        for ($i = 1; $i <= $this->structureCount; ++ $i) {
            /* Генерируем параметры Здания */
            $model = new Structure([
                'address' => $this->faker->streetAddress,
                'lat' => $this->faker->latitude,
                'lon' => $this->faker->longitude
            ]);
            if (!$model->save()) {
                continue;
            }
            echo "    > Current Structure ", $i, " of ", $this->structureCount, " (", Yii::$app->formatter->asPercent($i / $this->structureCount, 1), ")", "\r";
        }
        echo "\n    > Structures generated\n";
    }

    /**
     * Метод, генерирующий тестовые данные для Компаний
     */
    protected function generateCompanies()
    {
        Yii::$app->db->createCommand('TRUNCATE TABLE ' . Company::tableName() . ' CASCADE')->execute();
        Yii::$app->db->createCommand('ALTER SEQUENCE company_id_seq RESTART WITH 1')->execute();
        Yii::$app->sphinx->createCommand()->truncateIndex(CompanyRT::indexName())->execute();
        
        $rubricsLeaves = Rubric::find()->leaves()->all();
        
        echo "    > Generate Companies\n";
        $minIdStructure = Structure::find()->min('id');
        $maxIdStructure = Structure::find()->max('id');
        for ($i = 1; $i <= $this->companyCount; ++ $i) {
            $phones = [];
            for ($j = 0; $j < $this->faker->numberBetween(0, self::MAX_PHONES); ++ $j) {
                $phones[] = $this->faker->phoneNumber;
            }
            $model = null;
            /* Генерируем параметры Компании */
            $model = new Company([
                'name' => $this->faker->company,
                'structure_id' => $this->faker->numberBetween($minIdStructure, $maxIdStructure),
                'phones' => Json::encode($phones)
            ]);
            if (!$model->save()) {
                continue;
            }
            /*
             * Генерируем количество рубрик, которые будут присвоены компании. При этом вероятность
             * того, что у Компании будет более одной рубрики, составляет 15 %
             */
            $countRubrics = $this->faker->optional(15, 1)->numberBetween(2, self::MAX_RUBRICS_FOR_COMPANY);
            $rubrics = $rubricsLeaves;
            shuffle($rubrics);
            $rubrics = array_slice($rubrics, 0, $countRubrics);
            foreach ($rubrics as $rubric) {
                $model->link('rubrics', $rubric);
            }
            $model->saveModelInSphinxIndex();
            echo "    > Current Company ", $i, " of ", $this->companyCount, " (", Yii::$app->formatter->asPercent($i / $this->companyCount, 1), ")", "\r";
        }
        echo "\n    > Companies generated\n";
    }

    /**
     * Метод, импортирующий рубрики из файла
     */
    protected function importRubrics()
    {
        echo "    > Import Rubrics\n";
        Yii::$app->db->createCommand('TRUNCATE TABLE ' . Rubric::tableName() . ' CASCADE')->execute();
        Yii::$app->db->createCommand('ALTER SEQUENCE rubric_id_seq RESTART WITH 1')->execute();
        
        /* Файл, в котором хранятся рубрики для импорта */
        $rubrics = Json::decode(file_get_contents(__DIR__ . "/../config/rubrics.json"));
        
        foreach ($rubrics as $rubric) {
            $model = new Rubric([
                'name' => $rubric['name']
            ]);
            $model->makeRoot();
            $this->addChildRubric($model, $rubric);
        }
        echo "    > Rubrics imported\n";
    }

    /**
     * Метод, добавляющий рекурсивно для предка дочерние рубрики
     *
     * @param Rubric $parentRubric родительская рубрика, к оторой нужно добавить дочерние рубрики
     * @param array $data данные, содержащие в себе дочерние рубрики
     */
    protected function addChildRubric(Rubric $parentRubric, $data)
    {
        if (isset($data['items']) && is_array($data['items'])) {
            foreach ($data['items'] as $rubric) {
                $model = new Rubric([
                    'name' => $rubric['name']
                ]);
                $model->appendTo($parentRubric);
                $this->addChildRubric($model, $rubric);
            }
        }
    }

    /**
     * Запуск импорта данных. Через параметр structureCount можно указать, сколько необходимо
     * сгенерировать зданий. По умолчанию генерируется 100000 зданий. Через параметр companyCount
     * можно указать, сколько необходимо сгенерировать компаний. По умолчанию генерируется 100000
     * компаний.
     */
    public function actionIndex()
    {
        if (empty($this->structureCount)) {
            $this->structureCount = self::MAX_STRUCTURE;
        }
        if (empty($this->companyCount)) {
            $this->companyCount = self::MAX_COMPANY;
        }
        $this->faker = \Faker\Factory::create('ru_RU');
        $this->importRubrics();
        $this->generateStructures();
        $this->generateCompanies();
    }
}
